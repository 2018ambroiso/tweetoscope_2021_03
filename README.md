# Tweetoscope

The project description and guideline are listed [on this website](https://pennerath.pages.centralesupelec.fr/tweetoscope/index.html). 


## Description of the architecture
The final version of the application consist of the deployment of the following blocks :
- The Tweet Generator that generates tweets
- The Tweet Collector that organizes them as cascades
- The Hawkes Estimator that predicts the number of retweets
- The Learner that trains a random forest model in order to improve prediction 
- The Predictor that predicts the number of retweets, based on the results from Learner

## Prerequisites
You need to have minikube install.

## Installing

Download the 2 yaml file, then do :

`minikube start` <br />
`kubectl apply -f zookeeper-and-kafka.yml` <br />
`kubectl apply -f tweetoscope-deployment-v1.yml` <br />

You can check that your pods are running with : `kubectl get pods` <br />

You can check logs for any node with : `kubectl logs <name_of_pod>` <br />

In particular, the logger pod print the logs from all the node. You can test it with : `kubectl logs logger-pod` <br />

## Demo
You can find [here](https://www.dropbox.com/s/53hlt2enqz4je23/D%C3%A9mo%20Tweetoscope.mov?dl=0) the video showing how to use the app.





