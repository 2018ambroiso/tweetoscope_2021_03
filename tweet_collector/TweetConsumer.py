import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer

#All arguments are parsd in this file, this simplifies arguments retrieval
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--obs-windows',type=int, nargs='+',required=True, help="the time windows list")
parser.add_argument('--end-time', type=int, required=True,help="the time beyond which we consider a cascade as over")
args = parser.parse_args()  # Parse arguments

consumer = KafkaConsumer('tweets',                   # Topic name
  bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
  key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)

