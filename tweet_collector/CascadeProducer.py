import json                       # To parse and dump JSON
from kafka import KafkaProducer   # Import Kafka producder
import TweetConsumer
import sys 
sys.path.insert(1, '..')
import logger


#We create two producers, one will send the messages to the topic 'cascade_series' and the other to 'cascade_properties'
producer1 = KafkaProducer(
  bootstrap_servers = TweetConsumer.args.broker_list,                     # List of brokers passed from the command line
  value_serializer=lambda v: json.dumps(v).encode('utf-8') # How to serialize the value to a binary buffer
)

producer2 = KafkaProducer(
  bootstrap_servers = TweetConsumer.args.broker_list,                     # List of brokers passed from the command line
  value_serializer=lambda v: json.dumps(v).encode('utf-8'),
  key_serializer = lambda x : x.to_bytes(2, byteorder='big')
)


logger = logger.get_logger('Collector', broker_list=TweetConsumer.args.broker_list, debug=True) # Identify the node of origin of the message. 


#We load the parameters from the Params.py file
obs_windows = TweetConsumer.args.obs_windows  
end_time = TweetConsumer.args.end_time

class Processor :
    def __init__(self, src) :
        self.src = src
        self.series = {}
        self.sizes = {}
        self.first_timestamp = {} #Stock le premier timestamp pour chaque cascade
        self.timestamp = {} #Stock le dernier timestamp pour chaque cascade
        self.observation = {} #Stock un pointeur sur obs_windows pour chaque cascade
        self.time = 0 #Dernier timestamp enregistré par le processeur

    def add_tweet(self, msg) :
      #First we handle the serie
        serie = {}
        serie['type'] = 'serie'
        serie['cid'] = msg.value['tweet_id']
        serie['msg'] = msg.value['msg']
        serie['T_obs'] = None               #On ajoutera cette valeur une fois la cascade partielle envoyée.
        serie['tweets'] = [(msg.value['t'], msg.value['m'])]

      #Then we handle the size
        size = {}
        size['type'] = 'size'
        size['cid'] = msg.value['tweet_id']
        size['n_tot'] = 1                   #We initialize here the tweet counter
        size['t_end'] = msg.value['t']

        self.series[msg.value['tweet_id']] = serie
        self.sizes[msg.value['tweet_id']] = size

        self.first_timestamp[msg.value['tweet_id']] = msg.value['t']  # We store the time of the first tweet of the cascade
        self.timestamp[msg.value['tweet_id']] = msg.value['t']        # We store the last timestamp corresponding to the cascade
        self.time = msg.value['t']
        self.observation[msg.value['tweet_id']] = 0 # We initialize the pointer on the observation windows here

    def add_retweet(self, msg) :
        self.time = msg.value['t']
        if msg.value['tweet_id'] in self.series.keys() :   
          if (msg.value['t'] - self.series[msg.value['tweet_id']]['tweets'][0][0]) <= end_time : # We add retwteets until the end_time of the cascade
              self.series[msg.value['tweet_id']]['tweets'].append((msg.value['t'], msg.value['m'])) # We add the tweet to our serie
              self.sizes[msg.value['tweet_id']]['n_tot'] += 1  # We add the tweet to our counter
              self.sizes[msg.value['tweet_id']]['t_end'] = msg.value['t']
              self.timestamp[msg.value['tweet_id']] = msg.value['t']
    
    def send_finished_cascade(self) :
      #On appelle cette fonction à chaque fois que l'on reçoit un tweet. S'il s'est écoulé 
      #plus de end_time unités de temps entre le tweet et le dernier tweet d'une cascade, alors on la 
      #considère comme terminée et on l'envoie au topic 'cascade_properties'. On supprime ensuite
      #la cascade du producer.
      for cid, t in self.timestamp.items() :
        if cid in self.sizes.keys() : 
          if self.time - t > end_time : 
            #We send one message per time window
            for x in obs_windows:
              logger.info("Finished cascade n° " + str(cid) + " sent to cascade_properties, containing " + str(self.sizes[cid]["n_tot"]) + " retweets")
              producer2.send('cascade_properties', key = x, value = self.sizes[cid])
            del self.sizes[cid]

    def send_partial_cascade(self) : 
      for cid, t in self.timestamp.items() :
        if cid in self.series.keys():
          if self.time - t > end_time:
            del self.series[cid]              # We delete when the cascade is considered as finished
            del self.observation[cid]         # The following lines are here to avoid memory leak
            del self.first_timestamp[cid]     
            del self.timestamp[cid]
            return
          
          try:
            if t - self.first_timestamp[cid] > obs_windows[self.observation[cid]]:
              self.series[cid]['T_obs'] = obs_windows[self.observation[cid]]
              self.observation[cid] += 1     # The pointer will point to the next value
              logger.info("Partial cascade n° " + str(cid) + " sent to cascade_series, containing " + str(len(self.series[cid]['tweets'])) + " retweets")
              producer1.send('cascade_series', key = None, value = self.series[cid])

          except(IndexError):                # When self.observation[cid] gets out of index, we stop posting.
            pass


#On créé un dictionnaire qui possède comme clés les sources déjà existantes, 
#et comme valeurs les processeurs associés
sources = {}

for msg in TweetConsumer.consumer :
  #On check si la source du message est déjà associée à un processeur
  if msg.key in sources.keys() :
    a = sources[msg.key]
    if msg.value['type'] == 'tweet' :
      a.add_tweet(msg)
    else :
      a.add_retweet(msg)

    #On envoie à 'Cascade_properties' les cascades terminées
    a.send_finished_cascade()
    a.send_partial_cascade() 


  #S'il n'existe pas encore de processeur associé à cette source, on appelle la classe
  #pour en créer un nouveau, et on l'ajoute à "sources"
  else :
    a = Processor(msg.key)
    a.add_tweet(msg)
    sources[msg.key] = a

