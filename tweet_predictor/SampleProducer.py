import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaProducer   # Import Kafka producder
import CascadePropertiesConsumer
import ModelsConsumer
import logger

#Test Pipeline


producer = KafkaProducer(
  bootstrap_servers = CascadePropertiesConsumer.args.broker_list,                     # List of brokers passed from the command line
  value_serializer=lambda v: json.dumps(v).encode('utf-8'), # How to serialize the value to a binary buffer
  key_serializer = lambda x : x.to_bytes(2, byteorder='big')                               # How to serialize the key
)

logger = logger.get_logger("Predictor", broker_list=CascadePropertiesConsumer.args.broker_list, debug=True)


alpha = 2.3
mu = 10

parameters = {}
res_dic = {}
models_dic = {}

for msg in CascadePropertiesConsumer.consumer :
    if msg.value['type'] == 'parameters' :
      p = msg.value['parameters'][0]
      beta = msg.value['parameters'][1]
      g1 = msg.value['parameters'][2]
      nstar = p*mu*((alpha-1)/(alpha-2))
      try:
        parameters[(msg.value['cid'], msg.key)]['beta'] = beta
        parameters[(msg.value['cid'], msg.key)]['nstar'] = nstar
        parameters[(msg.value['cid'], msg.key)]['G1'] = g1
        parameters[(msg.value['cid'], msg.key)]['n'] = msg.value['n_obs']
      except(KeyError):
        parameters[(msg.value['cid'], msg.key)] = {'beta' : beta, 'nstar' : nstar, 'G1' : g1, 'n' : msg.value['n_obs']}

    if msg.value['type'] == 'size' :
      try :
        parameters[(msg.value['cid'], msg.key)]['N_tot'] = msg.value['n_tot']
      except(KeyError):
        parameters[(msg.value['cid'], msg.key)] = {'N_tot' : msg.value['n_tot']}
        
      
    if len(parameters[(msg.value['cid'], msg.key)].keys()) == 5: # If everything was received for this message and this T_obs
      beta,nstar,g1,n, N_tot = parameters[(msg.value['cid'],msg.key)]['beta'], parameters[(msg.value['cid'],msg.key)]['nstar'], parameters[(msg.value['cid'],msg.key)]['G1'],parameters[(msg.value['cid'], msg.key)]['n'], parameters[(msg.value['cid'], msg.key)]['N_tot']
      w = (N_tot/n) * (1-nstar)/g1
      res = {}
      samples = {}
      res['type'] = 'sample'
      res['cid'] = msg.value['cid']
      res['X'] = [beta, nstar, g1]
      res['W'] = w
      try :
        res_dic[msg.key]['res'] += res
      except(KeyError):
        res_dic[msg.key] = {'res' : [res]}
      producer.send('sample', key=msg.key, value=res)

      for model in ModelsConsumer.consumer: 
        models_dic[model.key] = model.value

      if (msg.key in models_dic.keys()):
        w_updated = models_dic[msg.key].predict([[beta,nstar,g1]])
        n_updated = int(n + w_updated[0] * ( g1/(1-nstar) ))
        logger.info("Tweet : {} ; Obs window : {} ; First prediction : {}; Forest prediction : {}; Real obs : {} ".format(msg.value['cid'], msg.key,parameters[(msg.value['cid'], msg.key)]['n'], n_updated,N_tot))
        