from kafka import KafkaConsumer
import CascadePropertiesConsumer
import pickle

consumer = KafkaConsumer('model',
bootstrap_servers = CascadePropertiesConsumer.args.broker_list,
value_deserializer = lambda m : pickle.loads(m),
key_deserializer=lambda x : int.from_bytes(x, "big"), 
auto_offset_reset="earliest",
consumer_timeout_ms=10000
)