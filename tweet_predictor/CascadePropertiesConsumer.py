import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--obs-windows',type=int, nargs='+',required=True, help="the time windows list")
args = parser.parse_args()  # Parse arguments

consumer = KafkaConsumer('cascade_properties',                   # Topic name
  bootstrap_servers = args.broker_list,                        # List of brokers passed from the command line
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),
  key_deserializer=lambda x : int.from_bytes(x, "big"),
  auto_offset_reset = 'earliest'            
)
